<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unit', function (Blueprint $table) {
            $table->id();
            $table->softDeletes();
            $table->string('name')->comment('分店名稱');
            $table->integer('Country')->comment('縣市');
            $table->integer('area')->comment('區');
            $table->string('address')->comment('地址');
            $table->integer('manager')->comment('店長');
            $table->string('tel')->comment('電話');
            $table->text('map')->comment('地圖');
            $table->string('email')->comment('信箱');
            $table->string('start')->comment('開始營業時間');
            $table->string('end')->comment('結束營業時間');
            $table->boolean('use')->default(1)->comment('啟用');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unit');
    }
}
