<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->id();
            $table->softDeletes();
            $table->string('uuid')->comment('uuid');
            $table->integer('unit')->default(0)->comment('分店別');
            $table->string('transid')->comment('交易號碼');
            $table->string('name')->comment('名稱');
            $table->string('password')->comment('密碼');
            $table->string('email')->comment('電子信箱');
            $table->boolean('administrator')->comment('系統管理');
            $table->boolean('is_use')->comment('使用狀態');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
