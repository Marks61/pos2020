<?php

use App\Models\Country;
use Illuminate\Database\Seeder;

class CountryDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Country::insert(['name'=>'臺北市']);
        Country::insert(['name'=>'新北市']);
        Country::insert(['name'=>'基隆市']);
        Country::insert(['name'=>'桃園市']);
        Country::insert(['name'=>'新竹市']);
        Country::insert(['name'=>'新竹縣']);
        Country::insert(['name'=>'苗栗縣']);
        Country::insert(['name'=>'臺中市']);
        Country::insert(['name'=>'彰化縣']);
        Country::insert(['name'=>'南投縣']);
        Country::insert(['name'=>'雲林縣']);
        Country::insert(['name'=>'嘉義市']);
        Country::insert(['name'=>'嘉義縣']);
        Country::insert(['name'=>'臺南市']);
        Country::insert(['name'=>'高雄市']);
        Country::insert(['name'=>'屏東縣']);
        Country::insert(['name'=>'宜蘭縣']);
        Country::insert(['name'=>'花蓮縣']);
        Country::insert(['name'=>'臺東縣']);
        Country::insert(['name'=>'澎湖縣']);
        Country::insert(['name'=>'金門縣']);
        Country::insert(['name'=>'連江縣']);
    }
}
