<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Hash;

class UserDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert(
            [
                'uuid' => Uuid::uuid4(),
                'transid' => 'U0001',
                'name' => 'admin',
                'password' => Hash::make('admin'),
                'email' => 'smaacg2011@gmail.con',
                'administrator'=>1,
                'is_use' => 1
            ]
        );
    }
}
