@extends('blade.view')
@section('main')
<!-- BREADCRUMB-->
<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">目前所在位置:</span>
                            <ul class="list-unstyled list-inline au-breadcrumb__list">
                                <li class="list-inline-item active">
                                    <a href="#">系統</a>
                                </li>
                                <li class="list-inline-item seprate">
                                    <span>/</span>
                                </li>
                                <li class="list-inline-item">操作紀錄</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="statistic">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <form id="syslog" name="syslog" action="{{url('/syslog')}}" method="POST">
                    <input type="text" hidden name="key" id="key" />
                    <input type="text" hidden name="Rows" id="Rows" />
                    <input type="text" hidden name="Page" id="Page" />
                    {{ csrf_field() }}
                </form>
                <div class="col-md-12">
                    <!-- DATA TABLE -->
                    <h3 class="title-5 m-b-35">資訊儀錶板</h3>
                    <div class="table-data__tool">
                        <div class="table-data__tool-left">
                            <div class="rs-select2--light rs-select2--sm">
                                <select class="js-select2" id="rows" onchange="poseter()" name="Rows">
                                    <option value="1" {{($nowRows == 1 ? 'selected="selected"' :'' )}}>1 Rows</option>
                                    <option value="5" {{($nowRows == 5 ? 'selected="selected"' :'' )}}>5 Rows</option>
                                    <option {{($nowRows == 10 ? 'selected="selected"' :'')}} value="10">10 Rows</option>
                                    <option value="25" {{($nowRows == 25 ? 'selected="selected"' :'')}}>25 Rows</option>
                                    <option value="50" {{($nowRows == 50 ? 'selected="selected"' :'')}}>50 Rows</option>
                                </select>
                                <div class="dropDownSelect2"></div>
                            </div>
                            @if($count!=0)
                            <div class="rs-select2--light rs-select2--sm">
                                <select class="js-select2" id="page" onchange="poseter()" name="Page">
                                    @php
                                    $i=0;
                                    @endphp
                                    @while ($i!=$page)
                                    @if($i!=0)
                                    <option value="{{$i}}" {{($nowPage == $i ? 'selected="selected"' :'' )}}>Page {{$i}}</option>
                                    @endif
                                    @php
                                    $i++;
                                    @endphp
                                    @endwhile
                                </select>
                                <div class="dropDownSelect2"></div>
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="table-responsive table-responsive-data2">
                        <table class="table table-data2">
                            <thead>
                                <tr>
                                    <th>日期</th>
                                    <th>記錄編號</th>
                                    <th>分店</th>
                                    <th>系統</th>
                                    <th>操作者</th>
                                    <th>內文</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($syslog as $rows)
                                <tr class="tr-shadow">
                                    <td>{{$rows->created_at}}</td>
                                    <td>
                                        {{$rows->id}}
                                    </td>
                                    <td>{{$rows->unit}}</td>
                                    <td>{{$rows->system}}</td>
                                    <td>
                                        {{$rows->Users['name']}}
                                    </td>
                                    <td>{{$rows->contact}}</td>
                                </tr>
                                @endforeach
                                @if($count==0)
                                <tr class="tr-shadow">
                                    <td colspan="6" align="center">查無資料</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- END DATA TABLE -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">
                                <p>Copyright © 2020 Colorlib.Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('script')
<script>
    $('#lock').submit(function(){
        return false;
    });
    function poseter()
    {
        $('#key').val($('#search').val());
        $('#Rows').val($('#rows').val());
        $('#Page').val($('#page').val());
        $('#syslog').submit();
    }
</script>
@stop
