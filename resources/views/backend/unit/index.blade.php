@extends('blade.view')
@section('main')
<!-- BREADCRUMB-->
<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">目前所在位置:</span>
                            <ul class="list-unstyled list-inline au-breadcrumb__list">
                                <li class="list-inline-item active">
                                    <a href="#">店鋪管理</a>
                                </li>
                                <li class="list-inline-item seprate">
                                    <span>/</span>
                                </li>
                                <li class="list-inline-item">店鋪列表</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="statistic">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <form id="unit" name="unit" action="{{url('/unit')}}" method="POST">
                    <input type="text" hidden name="key" id="key" />
                    <input type="text" hidden name="Rows" id="Rows" />
                    <input type="text" hidden name="Page" id="Page" />
                    <input type="hidden" id="nums" name="nums" value="{{$count}}" />
                    {{ csrf_field() }}
                </form>
                <div class="col-md-12">
                    <!-- DATA TABLE -->
                    <h3 class="title-5 m-b-35">資訊儀錶板</h3>
                    <div class="table-data__tool">
                        <div class="table-data__tool-left">
                            <div class="rs-select2--light rs-select2--sm">
                                <select class="js-select2" id="rows" onchange="poseter()" name="Rows">
                                    <option value="1" {{($nowRows == 1 ? 'selected="selected"' :'' )}}>1 Rows</option>
                                    <option value="5" {{($nowRows == 5 ? 'selected="selected"' :'' )}}>5 Rows</option>
                                    <option {{($nowRows == 10 ? 'selected="selected"' :'')}} value="10">10 Rows</option>
                                    <option value="25" {{($nowRows == 25 ? 'selected="selected"' :'')}}>25 Rows</option>
                                    <option value="50" {{($nowRows == 50 ? 'selected="selected"' :'')}}>50 Rows</option>
                                </select>
                                <div class="dropDownSelect2"></div>
                            </div>
                            @if($count!=0)
                            <div class="rs-select2--light rs-select2--sm">
                                <select class="js-select2" id="page" onchange="poseter()" name="Page">
                                    @php
                                    $i=0;
                                    @endphp
                                    @while ($i!=$page)
                                    @if($i!=0)
                                    <option value="{{$i}}" {{($nowPage == $i ? 'selected="selected"' :'' )}}>Page {{$i}}</option>
                                    @endif
                                    @php
                                    $i++;
                                    @endphp
                                    @endwhile
                                </select>
                                <div class="dropDownSelect2"></div>
                            </div>
                            @endif
                            <div class="rs-select2--dark rs-select2--sm rs-select2--dark2 more_btn">
                                <select class="js-select2" id="action" onchange="action();" name="type">
                                    <option selected="selected"  disabled hidden>批次操作</option>
                                    <option value="1">刪除</option>
                                    <option value="2">啟用項目</option>
                                    <option value="3">關閉項目</option>
                                </select>
                                <div class="dropDownSelect2"></div>
                            </div>
                        </div>
                        <div class="table-data__tool-right">
                            <button id="additem" onclick="location.href='{{url('/unit/create')}}'" class="au-btn au-btn-icon au-btn--green au-btn--small">
                                <i class="zmdi zmdi-plus"></i>新增</button>
                        </div>
                    </div>
                    <div class="table-responsive table-responsive-data2">
                        <table class="table table-data2">
                            <thead>
                                <tr>
                                    <th>
                                        <label class="au-checkbox">
                                            <input id="check_all" name="check" type="checkbox">
                                            <span class="au-checkmark"></span>
                                        </label>
                                    </th>
                                    <th>日期</th>
                                    <th>分店</th>
                                    <th>店長</th>
                                    <th>狀態</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($unit as $rows)
                                <tr class="tr-shadow">
                                    <td>
                                        <label class="au-checkbox">
                                            <input id="check_{{$rows->id}}" name="check[]" value="{{$rows->id}}" type="checkbox">
                                            <span class="au-checkmark subcheck"></span>
                                        </label>
                                    </td>
                                    <td>{{$rows->created_at}}</td>
                                    <td>
                                        {{$rows->name}}
                                    </td>
                                    <td>{{$rows->manager}}</td>
                                    <td><div id="vals{{$rows->id}}">{!!$rows->use == 1 ? '<span id="'.$rows->id.'" class="status--process">Opening</span>' :'<span id="'.$rows->id.'" class="status--denied">Close</span>' !!}</div></td>
                                    <td>
                                        <div class="table-data-feature">
                                            <button class="item" data-toggle="tooltip" onclick="location.href='{{url('/unit/edit/'.$rows->id)}}'" data-placement="top" title="修改">
                                                <i class="zmdi zmdi-edit"></i>
                                            </button>
                                            <button class="item" data-toggle="tooltip" onclick="deleter('{{$rows->id}}')" data-placement="top" title="刪除">
                                                <i class="zmdi zmdi-delete"></i>
                                            </button>
                                            <button class="item" data-toggle="tooltip" data-placement="top" onclick="change('{{$rows->id}}');" title="啟用/關閉">
                                                <i class="zmdi zmdi-more"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                @if($count==0)
                                <tr class="tr-shadow">
                                    <td colspan="6" align="center">查無資料</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- END DATA TABLE -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">
                                <p>Copyright © 2020 Colorlib.Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('script')
<script>
    $(document).ready(function(){
        $('.more_btn').hide();
        $('input[name="check[]"]').click(function(){

            var ck_box = false;
            $('input[name="check[]"]').each(function(){
                ck_box = (ck_box || $(this).prop('checked'));
            });

            if(ck_box)
            {
                $('.more_btn').show();
            }else
            {
                $('.more_btn').hide();
            }
        });

        $("#check_all").click(function() {
            if($("#check_all").prop("checked"))
            {
                $("input[name='check[]']").each(function() {
                    $(this).prop("checked", true);
                });
            } else
            {
                $("input[name='check[]']").each(function() {
                    $(this).prop("checked", false);
                });
            }

            if($('#nums').val()!='0')
            {
                if($("#check_all").prop("checked"))
                {
                    $('.more_btn').show();
                }else
                {
                    $('.more_btn').hide();
                }
            }
        });
    });

    $('#lock').submit(function(){
        return false;
    });

    function poseter()
    {
        $('#key').val($('#search').val());
        $('#Rows').val($('#rows').val());
        $('#Page').val($('#page').val());
        $('#unit').submit();
    }

    function change(id)
    {
        $.ajax({
            url: "{{url('/api/statusChange')}}?id="+id , //傳送目的地
            dataType: "json", //資料格式
            success: function(data) {
                $('#'+id).remove();
                if(data.use == 1)
                {
                    $('#vals'+id).append('<span id="'+id+'" class="status--process">Opening</span>');
                }else
                {
                    $('#vals'+id).append('<span id="'+id+'" class="status--denied">Close</span>');
                }
            },
            error: function(){
                alert('連線錯誤');
            }
        });
    }

    function deleter(id)
    {
        $.ajax({
            url: "{{url('/api/delete')}}?id="+id , //傳送目的地
            dataType: "json", //資料格式
            success: function(data) {
                if(data)
                {
                    $('#unit').submit();
                }
            },
            error: function(){
                alert('總店資料無法刪除!!');
            }
        });
    }

    function action()
    {
        if($('#action').val() == 2)
        {
            openMore();
        }else if($('#action').val() == 3)
        {
            closeMore();
        }else if($('#action').val() == 1)
        {
            deleteMore();
        }
    }

    function openMore()
    {
        var val=0;
        $('input[name="check[]"]:checkbox:checked').each(function(i) {
            val=this.value;
            $.ajax({
                url: "{{url('/api/open')}}?id="+val , //傳送目的地
                dataType: "json", //資料格式
                async:false,
                success: function(data) {
                    $('#'+val).remove();
                    $('#vals'+val).append('<span id="'+val+'" class="status--process">Opening</span>');
                },
                error: function(){
                    alert('連線錯誤');
                }
            });
        });
    }

    function closeMore()
    {
        var val=0;
        $('input[name="check[]"]:checkbox:checked').each(function(i) {
            val=this.value;
            $.ajax({
                url: "{{url('/api/close')}}?id="+val , //傳送目的地
                dataType: "json", //資料格式
                async:false,
                success: function(data) {
                    $('#'+val).remove();
                    $('#vals'+val).append('<span id="'+val+'" class="status--denied">Close</span>');
                },
                error: function(){
                    alert('連線錯誤');
                }
            });
        });
    }

    function deleteMore()
    {
        var val=0;
        $('input[name="check[]"]:checkbox:checked').each(function(i) {
            val=this.value;
            $.ajax({
                url: "{{url('/api/delete')}}?id="+val , //傳送目的地
                dataType: "json", //資料格式
                async:false,
                success: function(data) {
                    if(data)
                    {
                        $('#unit').submit();
                    }
                },
                error: function(){
                    alert('總店資料無法刪除!!');
                }
            });
        });
    }

</script>
@stop
