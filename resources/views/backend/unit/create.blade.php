@extends('blade.view')
@section('main')
<!-- BREADCRUMB-->
<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">目前所在位置:</span>
                            <ul class="list-unstyled list-inline au-breadcrumb__list">
                                <li class="list-inline-item active">
                                    <a href="#">店鋪管理</a>
                                </li>
                                <li class="list-inline-item seprate">
                                    <span>/</span>
                                </li>
                                <li class="list-inline-item">新增店鋪</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="statistic">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    @if($errors->any())
                    <div class="alert alert-danger" role="alert">
                        @foreach($errors->all() as $error)
                        <P>{{$error}}</P>
                        @endforeach
                    </div>
                    @endif
                    <div id="notfound" class="alert alert-danger" role="alert">
                        <P>查詢條件下找不到符合的帳號。</P>
                    </div>
                    <div id="notonly" class="alert alert-primary" role="alert">
                        <div id="notonlyform"></div>
                    </div>
                    <div class="card">
                        <form method="POST" action="{{url('/unit/insert')}}">
                            <div class="card-header">
                                <strong>店鋪管理</strong>
                                <small>新增</small>
                            </div>
                            <div class="card-body card-block">
                                <div class="form-group">
                                    <label for="name" class=" form-control-label">店鋪名稱</label>
                                    <input type="text" id="name" name="Unitname" placeholder="請輸入店鋪名稱" class="form-control">
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="country" class=" form-control-label">縣市選擇</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <select name="country" onchange="readVillage();changecountry(this.options[selectedIndex].text);" id="country" class="form-control">
                                            <option value="0">請選擇縣市</option>
                                            @foreach($country as $row)
                                            <option value="{{$row->id}}">{{$row->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="village" class=" form-control-label">鄉/鎮/區選擇</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <select name="village" onchange="changevillage(this.options[selectedIndex].text);" id="village" class="form-control">
                                            <option value="0">請選擇鄉/鎮/區域</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="street" class=" form-control-label">地址</label>
                                    <input type="hidden" id="countryname" value="" >
                                    <input type="hidden" id="villagename" value="" >
                                    <input type="text" name="street" id="street" onblur="changeAddress();" placeholder="請輸入地址，例如:德山街34號16樓" class="form-control">
                                </div>
                                <div class="row form-group">
                                    <div class="col mb-4 col-md-12 justify-content-start">
                                        <label for="manager" class=" form-control-label">分店長</label>
                                        <input type="text" name="manager" id="manager" placeholder="請輸入編號、姓名、或是電子信箱查詢" onblur="userSearch()" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="tel" class=" form-control-label">電話</label>
                                    <input type="text" id="tel" placeholder="電話" name="tel" class="form-control">
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="googlemap" class=" form-control-label">Google Map</label>
                                    </div>
                                    <div class="col-12 mb-4 col-md-9">
                                        <textarea name="googlemap" id="googlemap" rows="9" placeholder="Map..." class="form-control"></textarea>
                                    </div>
                                    <div id="viewmap" class="col-12 col-md-12">
                                        <iframe id="viewmaphtml" class="mb-2" width="800" height="600" frameborder="3" scrolling="no" marginheight="0" marginwidth="0" src=https://maps.google.com.tw/maps?f=q&hl=zh-TW&geocode=&q=臺北市羅斯福路二段102號&z=16&output=embed&t=></iframe>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="mail" class=" form-control-label">分店電子信箱</label>
                                    <input type="email" id="mail" placeholder="電子信箱" name="mail" class="form-control">
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-6">
                                        <label for="start" class="form-control-label">開始營業時間</label>
                                        <input type="text" value="" class="form-control" id="start" name="start" placeholder="00:00">
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <label for="end" class="form-control-label">結束營業時間</label>
                                        <input type="text" value="" class="form-control" id="end" name="end" placeholder="23:59">
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Submit
                                </button>
                                <button type="reset" class="btn btn-danger btn-sm" onclick="$('input').val('');$('#country').val(0);$('#village').val(0)">
                                    <i class="fa fa-ban"></i> Reset
                                </button>
                                <button type="button" onclick="resetManager()" class="btn btn-warning btn-sm">
                                    <i class="fa fa-ban"></i> ResetManager
                                </button>
                            </div>
                            {{ csrf_field() }}
                        </form>
                    </div>
                     <!-- END DATA TABLE -->
                     <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">
                                <p>Copyright © 2020 Colorlib.Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('script')
<script>
    $(document).ready(function(){
        $('#notfound').hide();
        $('#notonly').hide();
    });

    function changecountry(name){
        $('#countryname').val(name);
        $('#villagename').val('');
        if($('#villagename').val()!='' || $('#street').val() != ''){
            changeAddress();
        }else{
            $('#googlemap').text('<iframe id="viewmaphtml" class="mb-2" width="800" height="600" frameborder="3" scrolling="no" marginheight="0" marginwidth="0" src=https://maps.google.com.tw/maps?f=q&hl=zh-TW&geocode=&q='+name+'&z=16&output=embed&t=></iframe>');
        }
        $('#viewmaphtml').remove();
        $('#viewmap').append($('#googlemap').val());
    }

    function changevillage(name){
        $('#villagename').val(name);
        if($('#countryname').val()!='' || $('#street').val() != ''){
            changeAddress();
        }else{
            $('#googlemap').text('<iframe id="viewmaphtml" class="mb-2" width="800" height="600" frameborder="3" scrolling="no" marginheight="0" marginwidth="0" src=https://maps.google.com.tw/maps?f=q&hl=zh-TW&geocode=&q='+$('#countryname').val()+name+'&z=16&output=embed&t=></iframe>');
        }
        $('#viewmaphtml').remove();
        $('#viewmap').append($('#googlemap').val());
    }

    function changeAddress(){
        $('#googlemap').text('<iframe id="viewmaphtml" class="mb-2" width="800" height="600" frameborder="3" scrolling="no" marginheight="0" marginwidth="0" src=https://maps.google.com.tw/maps?f=q&hl=zh-TW&geocode=&q='+$('#countryname').val()+$('#villagename').val()+$('#street').val()+'&z=16&output=embed&t=></iframe>');
        $('#viewmaphtml').remove();
        $('#viewmap').append($('#googlemap').val());
    }

    function readVillage()
    {
        $.ajax({
            url: "{{url('/api/catchvillage')}}?country="+$('#country').val() , //傳送目的地
            dataType: "json", //資料格式
            success: function(data) {
                $('#village option').remove();
                $("#village").append("<option value='0'>請選擇鄉/鎮/區域</option>");
                 console.log(data);
                for(i=0;i<data.length;i++){
                    $("#village").append("<option value='"+data[i].id+"'>"+data[i].name+"</option>");
                }
            },
            error: function(){
                alert('連線錯誤');
            }
        });
    }

    function userSearch(){
        $.ajax({
            type:"POST",
            url: "{{url('/api/userSearch')}}" , //傳送目的地
            dataType: "json", //資料格式
            data:{
                users:$('#manager').val()
            },
            success: function(data) {
                if(data[0]['count'] == 0){
                    $('#notfound').show();
                }else if(data[0]['count'] == 1){
                    $('#manager').val(data[0]['data'][0].transid);
                    $('#manager').text(data[0]['data'][0].transid);
                    //$('#manager').attr('disabled','disabled');
                    $('#notfound').hide();
                }else if(data[0]['count'] > 1){
                    $("#notonlyform").remove();
                    $('#notonyform').append('<p>查詢條件下有多筆結果。請選擇對象。</p>');
                    for(i=0;i<data['data'].length;i++){
                        $("#notonlyform").append("<input type='link' onclick='insertManage("+data[i].transid+")' text='"+data[i].name+"'>");
                        $('#notonlyform').append('、');
                    }
                    $('#notfound').hide();
                }
            },
            error: function(){
                alert('連線錯誤');
            }
        });
    }

    function insertManage(name){
        $('#manager').val(name);
        $('#manager').text(name);
        //$('#manager').attr('disabled','disabled');
        $('#notonly').hide();
        $('#notfound').hide();
    }

    function resetManager(){
        $('#manager').val('');
        $('#manager').removeAttr('disabled');
    }

    $('#start').timeDropper(
        {
            meridians:true,
            format:'HH:mm',
        }
    );
    $('#end').timeDropper(
        {
            meridians:true,
            format:'HH:mm',
        }
    );
</script>
@stop
