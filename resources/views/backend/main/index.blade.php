@extends('blade.view')
@section('main')
<!-- BREADCRUMB-->
<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">目前所在位置:</span>
                            <ul class="list-unstyled list-inline au-breadcrumb__list">
                                <li class="list-inline-item active">
                                    <a href="#">主頁</a>
                                </li>
                                <li class="list-inline-item seprate">
                                    <span>/</span>
                                </li>
                                <li class="list-inline-item">儀錶板</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END BREADCRUMB-->

<!-- STATISTIC-->
<section class="statistic">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 col-lg-3">
                    <div class="statistic__item">
                        <h2 class="number">{{$member}}</h2>
                        <span class="desc">銷售員數量</span>
                        <div class="icon">
                            <i class="zmdi zmdi-account-o"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="statistic__item">
                        <h2 class="number">$388,688</h2>
                        <span class="desc">單日銷售業績</span>
                        <div class="icon">
                            <i class="zmdi zmdi-shopping-cart"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="statistic__item">
                        <h2 class="number">$1,086</h2>
                        <span class="desc">本週銷售業績</span>
                        <div class="icon">
                            <i class="zmdi zmdi-calendar-note"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="statistic__item">
                        <h2 class="number">$1,060,386</h2>
                        <span class="desc">單月銷售業績</span>
                        <div class="icon">
                            <i class="zmdi zmdi-money"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="table-data__tool">
                        <div class="table-data__tool-left">
                            <h2 class="title-1 m-b-25">系統操作紀錄</h2>
                        </div>
                        <div class="table-data__tool-right">
                            <button class="au-btn au-btn-load">Load More</button>
                        </div>
                    </div>
                    <div class="table-responsive table--no-card m-b-40">
                        <table class="table table-borderless table-striped table-earning">
                            <thead>
                                <tr>
                                    <th>日期</th>
                                    <th>紀錄編號</th>
                                    <th>分店</th>
                                    <th>系統</th>
                                    <th>操作者</th>
                                    <th class="text-right">內文</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($syslog5 as $rows)
                                <tr>
                                    <td>{{$rows->created_at}}</td>
                                    <td>{{$rows->id}}</td>
                                    <td>{{$rows->unit}}</td>
                                    <td>{{$rows->system}}</td>
                                    <td>{{$rows->Users['name']}}</td>
                                    <td class="text-right">{{$rows->contact}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12 col-lg-12">
                    <div class="user-data m-b-40">
                        <h3 class="title-3 m-b-30">
                            <i class="zmdi zmdi-account-calendar"></i>銷售紀錄</h3>
                        <div class="filters m-b-45">
                            <div class="rs-select2--dark rs-select2--md m-r-10 rs-select2--border">
                                <select class="js-select2" name="property">
                                    <option selected="selected">All Properties</option>
                                    <option value="">Products</option>
                                    <option value="">Services</option>
                                </select>
                                <div class="dropDownSelect2"></div>
                            </div>
                            <div class="rs-select2--dark rs-select2--sm rs-select2--border">
                                <select class="js-select2 au-select-dark" name="time">
                                    <option selected="selected">All Time</option>
                                    <option value="">By Month</option>
                                    <option value="">By Day</option>
                                </select>
                                <div class="dropDownSelect2"></div>
                            </div>
                        </div>
                        <div class="table-responsive table-data">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td>
                                            <label class="au-checkbox">
                                                <input type="checkbox">
                                                <span class="au-checkmark"></span>
                                            </label>
                                        </td>
                                        <td>name</td>
                                        <td>role</td>
                                        <td>type</td>
                                        <td></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <label class="au-checkbox">
                                                <input type="checkbox">
                                                <span class="au-checkmark"></span>
                                            </label>
                                        </td>
                                        <td>
                                            <div class="table-data__info">
                                                <h6>lori lynch</h6>
                                                <span>
                                                    <a href="#">johndoe@gmail.com</a>
                                                </span>
                                            </div>
                                        </td>
                                        <td>
                                            <span class="role admin">admin</span>
                                        </td>
                                        <td>
                                            <div class="rs-select2--trans rs-select2--sm">
                                                <select class="js-select2" name="property">
                                                    <option selected="selected">Full Control</option>
                                                    <option value="">Post</option>
                                                    <option value="">Watch</option>
                                                </select>
                                                <div class="dropDownSelect2"></div>
                                            </div>
                                        </td>
                                        <td>
                                            <span class="more">
                                                <i class="zmdi zmdi-more"></i>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="au-checkbox">
                                                <input type="checkbox" checked="checked">
                                                <span class="au-checkmark"></span>
                                            </label>
                                        </td>
                                        <td>
                                            <div class="table-data__info">
                                                <h6>lori lynch</h6>
                                                <span>
                                                    <a href="#">johndoe@gmail.com</a>
                                                </span>
                                            </div>
                                        </td>
                                        <td>
                                            <span class="role user">user</span>
                                        </td>
                                        <td>
                                            <div class="rs-select2--trans rs-select2--sm">
                                                <select class="js-select2" name="property">
                                                    <option value="">Full Control</option>
                                                    <option value="" selected="selected">Post</option>
                                                    <option value="">Watch</option>
                                                </select>
                                                <div class="dropDownSelect2"></div>
                                            </div>
                                        </td>
                                        <td>
                                            <span class="more">
                                                <i class="zmdi zmdi-more"></i>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="au-checkbox">
                                                <input type="checkbox">
                                                <span class="au-checkmark"></span>
                                            </label>
                                        </td>
                                        <td>
                                            <div class="table-data__info">
                                                <h6>lori lynch</h6>
                                                <span>
                                                    <a href="#">johndoe@gmail.com</a>
                                                </span>
                                            </div>
                                        </td>
                                        <td>
                                            <span class="role user">user</span>
                                        </td>
                                        <td>
                                            <div class="rs-select2--trans rs-select2--sm">
                                                <select class="js-select2" name="property">
                                                    <option value="">Full Control</option>
                                                    <option value="" selected="selected">Post</option>
                                                    <option value="">Watch</option>
                                                </select>
                                                <div class="dropDownSelect2"></div>
                                            </div>
                                        </td>
                                        <td>
                                            <span class="more">
                                                <i class="zmdi zmdi-more"></i>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="au-checkbox">
                                                <input type="checkbox">
                                                <span class="au-checkmark"></span>
                                            </label>
                                        </td>
                                        <td>
                                            <div class="table-data__info">
                                                <h6>lori lynch</h6>
                                                <span>
                                                    <a href="#">johndoe@gmail.com</a>
                                                </span>
                                            </div>
                                        </td>
                                        <td>
                                            <span class="role member">member</span>
                                        </td>
                                        <td>
                                            <div class="rs-select2--trans rs-select2--sm">
                                                <select class="js-select2" name="property">
                                                    <option selected="selected">Full Control</option>
                                                    <option value="">Post</option>
                                                    <option value="">Watch</option>
                                                </select>
                                                <div class="dropDownSelect2"></div>
                                            </div>
                                        </td>
                                        <td>
                                            <span class="more">
                                                <i class="zmdi zmdi-more"></i>
                                            </span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="user-data__footer">
                            <button class="au-btn au-btn-load">load more</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">
                                <p>Copyright © 2020 Colorlib.Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END STATISTIC-->
@stop
