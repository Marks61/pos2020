@extends('blade.account')
@section('main')
<div class="page-wrapper">
    <div class="page-content--bge5">
        <div class="container">
            <div class="login-wrap">
                <div class="login-content">
                    @if($errors->any())
                    <div class="alert alert-danger" role="alert">
                        @foreach($errors->all() as $error)
                        <P>{{$error}}</P>
                        @endforeach
                    </div>
                    @endif
                    @if(($admin!='' && !$errors->any()) || ($admin==='' && $message!=''))
                    <div class="alert {{$message !== '' ? 'alert-danger' : 'alert-primary'}}" role="alert">
                        <P>{{$message !=='' ? $message : '需要管理權限，請重新登入。'}}</P>
                    </div>
                    @endif
                    <div class="login-logo">
                        <a href="#">
                            <img src="{{url('/images/icon/logo.png')}}" alt="CoolAdmin">
                        </a>
                    </div>
                    <div class="login-form">
                        <input type="text" id="show" value="{{$modal}}" hidden>
                        <form action="{{$admin==='' ? url('/signin/root/null') : url('/signin/root/signup')}}" method="post">
                            <div class="form-group">
                                <label>銷售員代號</label>
                                <input class="au-input au-input--full" type="text" name="transid" placeholder="銷售員代號">
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input class="au-input au-input--full" type="password" name="password" placeholder="password">
                            </div>
                            <div class="login-checkbox">
                                <label>
                                    <a href="{{url('/signin/forgot')}}">Forgotten Password?</a>
                                </label>
                            </div>
                            <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">sign in</button>
                            {{ csrf_field() }}
                        </form>
                        @if($admin === '')
                        <div class="register-link">
                            <p>
                                點此註冊新銷售員
                                <a href="{{url('/signup')}}">Sign Up</a>
                            </p>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('modal')
<!-- modal large -->
<div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="largeModalLabel">登入失敗</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>{{$message}}</p>
		    </div>
			<div class="modal-footer">
				<button type="button" onclick="$('#largeModal').modal('hide');" class="btn btn-primary">確定</button>
			</div>
		</div>
	</div>
</div>
<!-- end modal large -->
@stop
@section('script')
<script>
    $(document).ready(function(){
        if($('#show').val()=='show')
        {
            $('#largeModal').modal('show');
        }
    });
</script>
@stop
