@extends('blade.account')
@section('main')
<div class="page-wrapper">
    <div class="page-content--bge5">
        <div class="container">
            <div class="login-wrap">
                <div class="login-content">
                    @if($errors->any())
                    <div class="alert alert-danger" role="alert">
                        @foreach($errors->all() as $error)
                        <P>{{$error}}</P>
                        @endforeach
                    </div>
                    @endif
                    <div class="login-logo">
                        <a href="#">
                            <img src="{{url('/images/icon/logo.png')}}" alt="CoolAdmin">
                        </a>
                    </div>
                    <div class="login-form">
                        <form action="{{url('/signup/create')}}" method="post">
                            <div class="form-group">
                                <label>銷售員代號</label>
                                <input class="au-input au-input--full" disabled value="{{$poster}}" type="text" id="transid" name="transid" placeholder="銷售員代號">
                            </div>
                            <div class="form-group">
                                <label>姓名</label>
                                <input class="au-input au-input--full" type="text" id="name" name="name" placeholder="姓名">
                            </div>
                            <div class="form-group">
                                <label>連絡電話</label>
                                <input class="au-input au-input--full" type="text" id="tel" name="tel" placeholder="連絡電話，格式:0912345678">
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input class="au-input au-input--full" type="email" id="mail" name="mail" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <select name="unit" id="unit" class="form-control">
                                    <option value="0">總店</option>
                                    <option value="1">二店</option>
                                    <option value="2">三店</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <select name="ad" id="select" class="form-control">
                                    <option value="0">請選擇身分</option>
                                    <option value="1">管理</option>
                                    <option value="2">一般</option>
                                </select>
                            </div>
                            <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">register</button>
                            {{ csrf_field() }}
                        </form>
                        <div class="register-link">
                            <p>
                                <a href="{{url('/signin')}}">返回登入</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
