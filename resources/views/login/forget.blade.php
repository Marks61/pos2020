@extends('blade.account')
@section('main')
<div class="page-wrapper">
    <div class="page-content--bge5">
        <div class="container">
            <div class="login-wrap">
                <div class="login-content">
                    @if($errors->any())
                    <div class="alert alert-danger" role="alert">
                        @foreach($errors->all() as $error)
                        <P>{{$error}}</P>
                        @endforeach
                    </div>
                    @endif
                    <div class="login-logo">
                        <a href="#">
                            <img src="{{asset('/public/images/logo.jpg')}}" alt="CoolAdmin">
                        </a>
                    </div>
                    <div class="login-form">
                       <form action="{{url('/checkMail')}}" method="POST">
                            <div class="form-group">
                                <label>Email Address</label>
                                <input class="au-input au-input--full" type="email" name="mail" placeholder="Email">
                            </div>
                            {{ csrf_field() }}
                            <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">submit</button>
                       </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
