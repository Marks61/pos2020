<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Unit extends Model
{
    use SoftDeletes;
    protected $table = 'unit';
    protected $primarykey = 'id';
    protected $fillable = ['Country', 'area', 'name', 'address', 'manager', 'email', 'tel', 'use', 'map', 'start', 'end'];
    protected $timestamp = true;

    public function manager()
    {
        return $this->belongsTo('App\Models\User','manager');
    }
}
