<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    use SoftDeletes;
    protected $table = 'user';
    protected $primarykey = 'id';
    protected $fillable = ['uuid', 'unit', 'name', 'transid', 'password', 'email', 'administrator', 'is_use'];
    protected $timestamp = true;

    public function syslog()
    {
        return $this->hasMany('App\Models\Syslog','users');
    }

    public function unit()
    {
        return $this->hasMany('App\Models\Unit','manager');
    }
}
