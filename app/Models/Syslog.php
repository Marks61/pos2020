<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Syslog extends Model
{
    protected $table = 'syslog';
    protected $primarykey = 'id';
    protected $fillable = ['uuid', 'unit', 'system', 'contact', 'users'];
    protected $timestamp = true;

    public function Users()
    {
        return $this->belongsTo('App\Models\User','users');
    }

}
