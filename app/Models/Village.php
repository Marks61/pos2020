<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Village extends Model
{
    protected $table = 'village';
    protected $primarykey = 'id';
    protected $fillable = ['country', 'name'];
    protected $timestamp = true;
}
