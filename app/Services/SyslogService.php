<?php

namespace App\Services;

use App\Repositories\SyslogRepository;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;

class SyslogService
{
    public function __construct()
    {
        $this->syslogRepo = new SyslogRepository;
    }

    /**
     * 寫入紀錄
     *
     * @param string $content
     * @param string $system
     * @return void
     */
    public function insert($content, $system)
    {
        $this->syslogRepo->insert($content, $system);
    }

    public function list($keys='')
    {
       return  $this->syslogRepo->list($keys);
    }
}
