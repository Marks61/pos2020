<?php

namespace App\Services;

use App\Repositories\UnitRepository;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;

class UnitService
{
    public function __construct()
    {
        $this->unitRepo = new UnitRepository;
    }

    /**
     * 新增資料
     *
     * @param string $content
     * @param string $system
     * @return void
     */
    public function insert($request)
    {
        $this->unitRepo->insert($request);
    }

    /**
     * 修改資料
     *
     * @param string $content
     * @param string $system
     * @return void
     */
    public function update($request,$id)
    {
        $this->unitRepo->update($request,$id);
    }

    public function list($keys='')
    {
       return  $this->unitRepo->list($keys);
    }
}
