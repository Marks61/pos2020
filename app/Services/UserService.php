<?php

namespace App\Services;

use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;

class UserService
{
    public function __construct()
    {
        $this->userRepo = new UserRepository;
    }

    /**
     * 確認帳號是否存在
     *
     * @param string $email
     * @return void
     */
    public function checkAccount($request, $action)
    {
        $auth = Auth::attempt(['transid' => $request->transid, 'password' => $request->password]);
        if ($auth) {
            if ($action === 'null') {
                return true;
            } else {
                if (!Auth::user()->administrator) {
                    Auth::logout();
                    return false;
                } else {
                    return true;
                }
            }
        } else {
            return false;
        }
    }

    public function insertAccount($request, $id)
    {
        $insert = $this->userRepo->insert($request, $id);
        if ($insert) {
            return true;
        } else {
            return false;
        }
    }

    public function searchManage($users)
    {
        $data = $this->userRepo->searchManage($users);
        return array(['count' => count($data), 'data' => $data]);
    }

    public function getId($users)
    {
        return $this->userRepo->getId($users);
    }

    public function list($keys='')
    {
       return  $this->userRepo->list($keys);
    }
}
