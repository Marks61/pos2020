<?php

namespace App\Http\Requests;

use App\Rules\Account;
use Illuminate\Foundation\Http\FormRequest;

class AccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tel' => 'required|max:15',
            'mail' => 'required|email',
            'name'=>'required',
            'ad' => new Account
        ];
    }

    public function messages()
    {
        return [
            'tel.required' => '連絡電話 不可留空。',
            'tel.max' => '連絡電話 最長不能超過15個字',
            'mail.required' => 'email 不可留空。',
            'mail.email' => 'email 必須符合正確格式。',
            'name.required'=>'姓名 不可留空。'
        ];
    }
}
