<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UnitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Unitname' => 'required',
            'country' => ['required'],
            'village' => ['required'],
            'street' => 'required',
            'manager' => 'required',
            'tel' => 'required|numeric',
            'googlemap' => 'required',
            'mail' => 'required|email',
            'start' => 'required|date_format:H:i',
            'end' => 'required|date_format:H:i'
        ];
    }

    public function messages()
    {
        return [
            'Unitname.required' => '店鋪名稱 不可留空。',
            'country.required' => '縣市 不可留空。',
            'village.required' => '鄉/鎮/區 不可留空。',
            'street.required' => '街道住址 不可留空。',
            'manager.required' => '分店長編號 不可留空。',
            'tel.required' => '分店電話 不可留空。',
            'tel.numeric' => '分店電話 不須是數字。',
            'googlemap.required' => '地圖 不可留空。',
            'mail.required' => '電子郵件 不可留空。',
            'mail.email' => '電子郵件 必須符合正確格式。',
            'start.required' => '營業時間-起 不可留空。',
            'start.date_format' => '營業時間-起 必須是正確時間格式。',
            'end.required' => '營業時間-迄 不可留空。',
            'end.date_format' => '營業時間-迄 必須是正確時間格式。'
        ];
    }
}
