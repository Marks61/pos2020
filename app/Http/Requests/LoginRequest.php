<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'transid' => 'required',
            'password' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'transid.required' => '銷售員代號 不可留空',
            'password.required' => '收銀機密碼 不可留空'
        ];
    }
}
