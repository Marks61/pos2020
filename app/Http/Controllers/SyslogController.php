<?php

namespace App\Http\Controllers;

use App\Services\SyslogService;
use Illuminate\Http\Request;

class SyslogController extends Controller
{
    public function __construct()
    {
        $this->SyslogService = new SyslogService;
    }

    public function index(Request $request)
    {
        $rows = 10;
        $page = 1;
        if (isset($request->Rows)) {
            $rows = $request->Rows;
        }
        if (isset($request->Page)) {
            $page = $request->Page;
        }
        return view('backend.Syslog.index', [
            'search'=>'true',
            'key'=>$request->key,
            'syslog' => $this->SyslogService->list($request->key)['data']->skip($page == 1 ? 0 : $rows * ($page - 1))->take($rows),
            'page' => (ceil(intval($this->SyslogService->list($request->key)['count']) / $rows) == 0 ? 1 : ceil(intval($this->SyslogService->list($request->key)['count']) / $rows) + 1),
            'nowRows'=>$rows,
            'nowPage'=>$page,
            'count'=>intval($this->SyslogService->list($request->key)['count'])
        ]);
    }
}
