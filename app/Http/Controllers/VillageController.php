<?php

namespace App\Http\Controllers;

use App\Repositories\VillageRepository;
use Illuminate\Http\Request;

class VillageController extends Controller
{
    public function __construct()
    {
        $this->VillageRepo=new VillageRepository;
    }

    public function list(Request $request)
    {
        return $this->VillageRepo->list($request->country);
    }
}
