<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\SyslogService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MainController extends Controller
{

    public function __construct()
    {
        $this->SyslogService = new SyslogService;
    }
    
    public function index()
    {
        return view('backend.main.index', [
            'member' => User::get()->count() - 1,
            'syslog5' => $this->SyslogService->list()['data']->take(5)
        ]);
    }
}
