<?php

namespace App\Http\Controllers;

use App\Http\Requests\UnitRequest;
use App\Models\Unit;
use App\Models\User;
use App\Services\UnitService;
use Illuminate\Http\Request;
use App\Repositories\CountryRepository;
use App\Repositories\VillageRepository;
use App\Services\SyslogService;

class UnitController extends Controller
{
    public function __construct()
    {
        $this->UnitService = new UnitService;
        $this->CountryRepo = new CountryRepository;
        $this->VillageRepo = new VillageRepository;
        $this->system = '店鋪管理';
        $this->SyslogService = new SyslogService;
    }

    public function index(Request $request)
    {
        $rows = 10;
        $page = 1;
        if (isset($request->Rows)) {
            $rows = $request->Rows;
        }
        if (isset($request->Page)) {
            $page = $request->Page;
        }
        return view('backend.Unit.index', [
            'search' => 'true',
            'key' => $request->key,
            'unit' => $this->UnitService->list($request->key)['data']->skip($page == 1 ? 0 : $rows * ($page - 1))->take($rows),
            'page' => (ceil(intval($this->UnitService->list($request->key)['count']) / $rows) == 0 ? 1 : ceil(intval($this->UnitService->list($request->key)['count']) / $rows) + 1),
            'nowRows' => $rows,
            'nowPage' => $page,
            'count' => intval($this->UnitService->list($request->key)['count'])
        ]);
    }

    public function createPage()
    {
        return view('backend.Unit.create', ['country' => $this->CountryRepo->list()]);
    }

    public function insertUnit(UnitRequest $request)
    {
        $this->SyslogService->insert('新增店鋪資訊:' . $request->Unitname, $this->system);
        $this->UnitService->insert($request);
        return redirect('/unit');
    }

    public function editPage($id)
    {
        return view('backend.Unit.edit', [
            'country' => $this->CountryRepo->list(),
            'data' => Unit::find($id),
            'village' => $this->VillageRepo->list(Unit::find($id)->Country),
            'countryName' => $this->CountryRepo->countryName(Unit::find($id)->Country),
            'villageName' => $this->VillageRepo->villageName(Unit::find($id)->area),
            'manager' => User::find(Unit::find($id)->manager)->transid
        ]);
    }

    public function update(UnitRequest $request, $id)
    {
        $this->SyslogService->insert('修改店鋪資訊:' . $request->Unitname, $this->system);
        $this->UnitService->update($request, $id);
        return redirect('/unit');
    }

    public function changeStatus(Request $request)
    {
        $data = Unit::find($request->id);
        $this->SyslogService->insert('更新店鋪狀態:' . $data->Unitname, $this->system);
        if ($data->use == 1) {
            $data->use = 0;
            $data->touch();
        } else {
            $data->use = 1;
            $data->touch();
        }
        return $data;
    }

    public function open(Request $request)
    {
        $data = Unit::find($request->id);
        $this->SyslogService->insert('更新店鋪狀態:' . $data->Unitname, $this->system);

        $data->use = 1;
        $data->touch();

        return $data;
    }

    public function close(Request $request)
    {
        $data = Unit::find($request->id);
        $this->SyslogService->insert('更新店鋪狀態:' . $data->Unitname, $this->system);

        $data->use = 0;
        $data->touch();

        return $data;
    }

    public function delete(Request $request)
    {
        $data = Unit::find($request->id);

        if ($data->id != 1) {
            $this->SyslogService->insert('刪除店鋪:' . $data->Unitname, $this->system);
            $data->delete();
            return true;
        } else {
            return false;
        }
    }
}
