<?php

namespace App\Http\Controllers;

use App\Http\Requests\AccountRequest;
use App\Http\Requests\LoginRequest;
use App\Models\User;
use App\Services\SyslogService;
use App\Services\UserService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    public function __construct()
    {
        $this->UserService = new UserService;
        $this->system = '帳號系統';
        $this->SyslogService = new SyslogService;
    }

    public function index($danger = false)
    {
        if (!$danger) {
            return view(
                'login.index',
                [
                    'modal' => '',
                    'admin' => '',
                    'message' => ''
                ]
            );
        } else {
            return view(
                'login.index',
                [
                    'modal' => '',
                    'admin' => '',
                    'message' => '帳號驗證錯誤，請重新驗證。'
                ]
            );
        }
    }

    public function searchManage(Request $request)
    {
        return $this->UserService->searchManage($request->users);
    }

    public function signup($danger = false)
    {
        if (!$danger) {
            return view(
                'login.index',
                [
                    'modal' => '',
                    'admin' => 'admin',
                    'message' => ''
                ]
            );
        } else {
            return view(
                'login.index',
                [
                    'modal' => '',
                    'admin' => 'admin',
                    'message' => '帳號驗證錯誤，或是沒有權限。請重新驗證。'
                ]
            );
        }
    }

    public function checkAccount(LoginRequest $request, $action)
    {
        $check = $this->UserService->checkAccount($request, $action);
        if ($action !== 'null') {
            if ($check) {
                $this->SyslogService->insert('權限驗證成功。', $this->system);
                return Redirect('/signup/insert');
            } else {
                $this->SyslogService->insert('權限驗證錯誤。', $this->system);
                return Redirect('/signup/1');
            }
        } else {
            if ($check) {
                if ($request->password == '00000000') {
                    Auth::logout();
                    return Redirect('/firstLogin');
                }
                $this->SyslogService->insert('登入系統。', $this->system);
                return Redirect('/backend');
            } else {
                $this->SyslogService->insert('異常登入。', $this->system);
                return Redirect('/signin/1');
            }
        }
    }

    public function insert(AccountRequest $request)
    {
        $number = User::whereRaw('Date(created_at) = CURDATE()')->get()->count() + 1;
        $poster = 'u' . date('ymd', strtotime(Carbon::now())) . str_pad($number, 2, '0', STR_PAD_LEFT);
        $insert = $this->UserService->InsertAccount($request, $poster);
        if ($insert) {
            $this->SyslogService->insert('註冊新帳號', $this->system);
            return Redirect('/signin');
        }
    }

    public function insertAccount()
    {
        $number = User::whereRaw('Date(created_at) = CURDATE()')->get()->count() + 1;
        $poster = 'u' . date('ymd', strtotime(Carbon::now())) . str_pad($number, 2, '0', STR_PAD_LEFT);
        return view('login.signup', [
            'poster' => $poster
        ]);
    }

    public function nologin()
    {
        Auth::logout();
        return view('login.nologin');
    }

    public function list(Request $request)
    {
        $rows = 10;
        $page = 1;
        if (isset($request->Rows)) {
            $rows = $request->Rows;
        }
        if (isset($request->Page)) {
            $page = $request->Page;
        }
        return view('backend.user.index', [
            'search' => 'true',
            'key' => $request->key,
            'unit' => $this->UserService->list($request->key)['data']->skip($page == 1 ? 0 : $rows * ($page - 1))->take($rows),
            'page' => (ceil(intval($this->UserService->list($request->key)['count']) / $rows) == 0 ? 1 : ceil(intval($this->UserService->list($request->key)['count']) / $rows) + 1),
            'nowRows' => $rows,
            'nowPage' => $page,
            'count' => intval($this->UserService->list($request->key)['count'])
        ]);
    }
}
