<?php

namespace App\Repositories;

use App\Models\Country;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Ramsey\Uuid\Uuid;

class CountryRepository
{
    public function list()
    {
        return Country::orderBy('id', 'asc')->get();
    }

    public function countryName($id)
    {
        return Country::find($id)->name;
    }
}
