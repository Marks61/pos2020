<?php

namespace App\Repositories;

use App\Models\Syslog;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Ramsey\Uuid\Uuid;

class SyslogRepository
{
    public function insert($content, $system)
    {
        return Syslog::insert([
            'uuid' => Uuid::uuid4(),
            'unit' => auth::check() ? Auth::user()->unit : 'system',
            'system' => $system,
            'contact' => $content,
            'users' => auth::check() ? Auth::user()->id : '0',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }

    public function list($keys = '')
    {
        $users = User::where('name', $keys)->get();
        if (count($users) > 0) {
            $userin = [];
            foreach ($users as $data) {
                $userin[] = $data->id;
            }
            $syslog = Syslog::with('Users')->whereIn('users', $userin)->get();
        } else if ($keys != '') {
            $syslog = Syslog::with('Users')->whereRaw('cast(created_at as char) like "%' . $keys . '%"')
                ->orwhere('system', 'like', '%'.$keys.'%')
                ->orwhere('contact', 'like', '%'.$keys.'%')->get();
        } else {
            $syslog = Syslog::with('Users')->get();
        }

        return array('count' => count($syslog), 'data' => $syslog);
    }
}
