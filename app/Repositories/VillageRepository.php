<?php

namespace App\Repositories;

use App\Models\Village;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Ramsey\Uuid\Uuid;

class VillageRepository
{
    public function list($country)
    {
        return Village::where('country', $country)->orderBy('id', 'asc')->get();
    }

    public function villageName($id)
    {
        return Village::find($id)->name;
    }

}
