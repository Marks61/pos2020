<?php

namespace App\Repositories;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Ramsey\Uuid\Uuid;

class UserRepository
{
    public function insert($data, $id)
    {
        return User::insert([
            'uuid' => Uuid::uuid4(),
            'unit' => $data->unit,
            'name' => $data->name,
            'transid' => $id,
            'password' => Hash::make('00000000'),
            'email' => $data->mail,
            'administrator' => $data->ad == 1 ? 1 : 0,
            'is_use' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }

    public function searchManage($key)
    {
        return User::where('transid', $key)->orwhere('name', $key)->orwhere('email', $key)->get();
    }

    public function getId($user)
    {
        return User::where('transid',$user)->first()->id;
    }
}
