<?php

namespace App\Repositories;

use App\Models\Unit;
use App\Models\User;
use App\Services\UserService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Ramsey\Uuid\Uuid;

class UnitRepository
{
    public function __construct()
    {
        $this->UserService = new UserService;
    }

    public function insert($request)
    {
        return Unit::insert([
            'Country' => $request->country,
            'area' => $request->village,
            'name' => $request->Unitname,
            'address' => $request->street,
            'manager' => $this->UserService->getId($request->manager),
            'email' => $request->mail,
            'tel' => $request->tel,
            'map' => $request->googlemap,
            'use' => 1,
            'start' => $request->start,
            'end' => $request->end,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }

    public function update($request, $id)
    {
        return Unit::where('id', $id)->update([
            'Country' => $request->country,
            'area' => $request->village,
            'name' => $request->Unitname,
            'address' => $request->street,
            'manager' => $this->UserService->getId($request->manager),
            'email' => $request->mail,
            'tel' => $request->tel,
            'map' => $request->googlemap,
            'use' => 1,
            'start' => $request->start,
            'end' => $request->end,
            'updated_at' => Carbon::now()
        ]);
    }

    public function list($keys = '')
    {
        if ($keys != '') {
            $unit = Unit::with('manager')->whereRaw('cast(created_at as char) like "%' . $keys . '%"')
                ->orwhere('name', 'like', '%' . $keys . '%')->get();
        } else {
            $unit = Unit::with('manager')->get();
        }

        return array('count' => count($unit), 'data' => $unit);
    }
}
