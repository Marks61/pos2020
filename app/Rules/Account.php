<?php

namespace App\Rules;

use App\Models\User;
use Illuminate\Contracts\Validation\Rule;

class Account implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->error = '';
    }

    public function emailCheck()
    {
        $data = request()->all()['mail'];
        $check = User::where('email', $data)->get();
        if (count($check) > 0) {
            $this->error = '銷售員資料已經存在。';
            return false;
        }
        return true;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        if ($value == 0 || !($value == 2 || $value == 1)) {
            $this->error = '權限設定錯誤';
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->error;
    }
}
