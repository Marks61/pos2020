<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'signin'], function () {
    Route::any('/{danger?}', 'UserController@index');
    Route::post('/root/{action?}', 'UserController@checkAccount');
});
Route::group(['prefix' => 'signup'], function () {
    Route::any('/insert', 'UserController@insertAccount');
    Route::post('/create', 'UserController@insert');
    Route::any('/{danger?}', 'UserController@signup');
});

Route::group(['middleware' => 'authcheck'], function () {
    Route::group(['prefix' => 'backend'], function () {
        Route::any('/', 'MainController@index');
    });
    Route::any('/syslog','SyslogController@index');
    Route::group(['prefix' => 'unit'], function () {
        Route::any('/','UnitController@index');
        Route::any('/create','UnitController@createPage');
        Route::post('/insert','UnitController@insertUnit');
        Route::any('/edit/{id}','UnitController@editPage');
        Route::post('/update/{id}','UnitController@update');
    });
});

Route::any('/nologin','UserController@nologin');
