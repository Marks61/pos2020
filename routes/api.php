<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/catchvillage','VillageController@list');
Route::post('/userSearch','UserController@searchManage');
Route::get('/statusChange','UnitController@changeStatus');
Route::get('/open','UnitController@open');
Route::get('/close','UnitController@close');
Route::get('/delete','UnitController@delete');

